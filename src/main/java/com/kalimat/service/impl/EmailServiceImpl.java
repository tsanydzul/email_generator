package com.kalimat.service.impl;

import java.sql.Connection;
import java.sql.DriverManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kalimat.dao.EmailDao;
import com.kalimat.service.EmailService;

@Service
public class EmailServiceImpl implements EmailService{
	
	@Autowired
	private EmailDao dao;
	
	@Override
	public String insert(String username) throws Exception {
		// TODO Auto-generated method stub
		Connection conn = null;
		String pertama = null;
		String kedua = null;
		String email = null;
		String hasilCommit = null;
		int cek = 0;
		try {
			
			
			String dbName = "test";
			String dbUserName = "monty";
			String dbPassword = "some_pass";
			String connectionString = "jdbc:mysql://localhost/" + dbName + "?user=" + dbUserName + "&password=" + dbPassword + "&useUnicode=true&serverTimezone=UTC";
			Class.forName("com.mysql.cj.jdbc.Driver");  
			conn = DriverManager.getConnection(connectionString);
			conn.setAutoCommit(false);
			
			String[] hasil = username.split(" ");
			
			if(hasil.length == 1) {
				email = username;
				
				cek = dao.readData(email, conn);
				if(cek != 0) {
					email = username+cek+"@kalimat.ai";
					hasilCommit = dao.insertData(username, email, conn);
				} else {
					email = username+"@kalimat.ai";
					hasilCommit = dao.insertData(username, email, conn);
				}
			} else {
				pertama = hasil[0];
				if(hasil.length == 2) {
					kedua = hasil[1];
				} else {
					kedua = hasil[hasil.length-1];
				}
				email = pertama+"."+kedua;
				
				cek = dao.readData(email, conn);
				if(cek != 0) {
					email = pertama+"."+kedua+cek+"@kalimat.ai";
					hasilCommit = dao.insertData(username, email, conn);
				} else {
					email = pertama+"."+kedua+"@kalimat.ai";
					hasilCommit = dao.insertData(username, email, conn);
				}
			}
			conn.commit();
		} catch (Exception e) {
			// TODO: handle exception
			conn.rollback();
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		return hasilCommit;
	}
	
}
