package com.kalimat;

public class EmailModel {
	private String username;

	public EmailModel() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EmailModel(String username) {
		super();
		this.username = username;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	} 
}
