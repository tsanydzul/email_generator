package com.kalimat.dao;

import java.sql.Connection;

public interface EmailDao {
	public String insertData(String name, String Email, Connection conn) throws Exception;
	public int readData(String email,Connection conn) throws Exception;
}
