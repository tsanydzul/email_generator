package com.kalimat.dao.impl;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.springframework.stereotype.Repository;

import com.kalimat.Query;
import com.kalimat.dao.EmailDao;
import com.mysql.cj.protocol.Resultset;

@Repository
public class EmailDaoImpl implements EmailDao{
	
	@Override
	public String insertData(String name, String email, Connection conn) throws Exception {
		// TODO Auto-generated method stub
		PreparedStatement ps = null;
		String hasil = null;
		try {
//			Email aidi = new Email();
//			int id  = aidi.getId();
					
			String query = Query.insert;
			ps = conn.prepareStatement(query);
			ps.setString(1, name);
			ps.setString(2, email);
//			ps.setInt(3, id);
			ps.executeUpdate();
			hasil = email;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} 
		
		return hasil;
	}

	
	@Override
	public int readData(String email, Connection conn) throws Exception {
		// TODO Auto-generated method stub
		PreparedStatement ps = null;
		ResultSet rs = null;
		int cek = 0;
		try {
			String query = Query.cekData;
			ps = conn.prepareStatement(query);
			ps.setString(1, email+"%");
			rs = ps.executeQuery();
			while(rs.next()) {
				cek = rs.getInt(1);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception();
		}
		return cek;
	}

}
