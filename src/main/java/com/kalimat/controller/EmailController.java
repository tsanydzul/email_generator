package com.kalimat.controller;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kalimat.EmailModel;
import com.kalimat.service.EmailService;

@RestController
public class EmailController {
	
	@Autowired
	private EmailService service;
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public <T>ResponseEntity<T> insert(@RequestBody EmailModel username){
		ResponseEntity<T> entity = null;
		HashMap<String, String> hashMap = new HashMap<>();
		String hasil = null;
		try {
			hasil = service.insert(username.getUsername());
			hashMap.put("email", hasil);
			entity = new ResponseEntity<T>((T) hashMap, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			e.getMessage();
			entity = new ResponseEntity<T>((T) e.getMessage(), HttpStatus.OK);
		}
		return entity;
	}
}
